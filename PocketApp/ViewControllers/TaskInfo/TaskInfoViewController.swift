//
//  TaskInfoViewController.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/30/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit
import UserNotifications

class TaskInfoViewController: UIViewController, UIScrollViewDelegate {
  
  var table: UITableView?

  let dataStore: DataStoreProtocol = FirebaseDataStore()
  var currentTaskID: String

  let dateformatter = DateFormatter()
  
  init(taskId: String) {
    self.currentTaskID = taskId
    super.init(nibName: nil, bundle: nil)
  }
  
  private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    self.currentTaskID = "\(-1)"
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  internal required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    taskInfoTextView.delegate = self
    scrollView.delegate = self
    self.view.addGestureRecognizer(tappedAway)
    self.table!.reloadData()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(false)
    self.table?.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    registerNotifications()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    unregisterNotifications()
  }

  //registerNotifications is for focus on textView
  func registerNotifications() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillShow),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillHide),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillChange),
                                           name: UIResponder.keyboardWillChangeFrameNotification,
                                           object: nil)
  }
  
  func unregisterNotifications() {
    NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
      return
    }
    scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    scrollView.contentInset.bottom = 0
  }
  
  var keyboardHeightLayoutConstraint: NSLayoutConstraint?
  @objc func keyboardWillChange(notification: NSNotification) {
    if let userInfo = notification.userInfo {
      let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
      let endFrameY = endFrame!.origin.y ?? 0
      let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
      let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
      let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
      let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
      if endFrameY >= UIScreen.main.bounds.size.height {
        self.keyboardHeightLayoutConstraint?.constant = 0.0
      } else {
        self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
      }
      UIView.animate(withDuration: duration,
                     delay: TimeInterval(0),
                     options: animationCurve,
                     animations: { self.view.layoutIfNeeded() },
                     completion: nil)
    }
  }
 
  @objc func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func dismissCellKeyboard(cell: UITableViewCell) {
    view.endEditing(true)
  }
  
  func setupTable() {
    let table = createTableView()
    position(table: table)
    self.table = table
  }
  
  func createTableView() -> UITableView {
    let table = UITableView()
    table.register(DetailsCell.self, forCellReuseIdentifier: DetailsCell.reuseID)
    table.register(IsDoneCell.self, forCellReuseIdentifier: IsDoneCell.reuseID)
    table.register(ScheduleDateCell.self, forCellReuseIdentifier: ScheduleDateCell.reuseID)
    table.register(NotificationsCell.self, forCellReuseIdentifier: NotificationsCell.reuseID)
    table.register(ParticipantFieldCell.self, forCellReuseIdentifier: ParticipantFieldCell.reuseID)
    table.dataSource = self
    table.delegate = self
    table.rowHeight = UITableView.automaticDimension
    table.estimatedRowHeight = 100
    table.separatorStyle = .none
    table.bounces = false
    table.isScrollEnabled = false
    return table
  }
  
  var constraint = NSLayoutConstraint()
  
  func position(table: UITableView) {
    viewInsideScroll.addSubview(table)
    table.translatesAutoresizingMaskIntoConstraints = false
    
    constraint = table.heightAnchor.constraint(equalToConstant: 100)
    
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: taskInfoTextView.bottomAnchor, constant: 20),
      table.leadingAnchor.constraint(equalTo: viewInsideScroll.leadingAnchor),
      table.trailingAnchor.constraint(equalTo: viewInsideScroll.trailingAnchor),
      table.bottomAnchor.constraint(equalTo: viewInsideScroll.bottomAnchor),
      constraint
    ])
  }
  
  func createNotificaiton() {
      let content = UNMutableNotificationContent()
      content.title = "\(currentTaskID)"
    
      dataStore.fetchOne(currentTaskID, { fetchedTask in
        guard let task = fetchedTask else { return }
        content.subtitle = "Task label: \(task.taskLabel)"
      })
      content.body = "Time has come"
      content.sound = UNNotificationSound.default
    
      dataStore.fetchOne(currentTaskID) { fetchedTask in
        if let task = fetchedTask {
          guard let scheduledDate = task.scheduledDate else { return }
          let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: scheduledDate)
          let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
          let notifIdentifier = String(self.currentTaskID)
          let request = UNNotificationRequest(identifier: notifIdentifier, content: content, trigger: trigger)
          
          UNUserNotificationCenter.current().add(request) { (error) in
            print(error as Any)
          }
        }
      }
  }
  
  func disableNotification() {
    UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
      var identifiers: [String] = []
      for notification:UNNotificationRequest in notificationRequests {
        if notification.identifier == "Identifier" {
          identifiers.append(notification.identifier)
        }
      }
      UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
    }

  }
  
  @objc func deleteTaskButton() {
    let deleteAlert = UIAlertController(title: "Delete task",
                                        message: "Are you sure? All data will be deleted.",
                                        preferredStyle: UIAlertController.Style.alert)
    deleteAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in

      self.dataStore.fetchOne(self.currentTaskID, { fetchedTask in
        guard let taskToDelete = fetchedTask?.taskID else { return }
        self.dataStore.deleteOne(taskToDelete, { boolean in
          
        })
        self.navigationController?.popViewController(animated: true)
      })
      
    }))
    
    deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
    }))
    
    present(deleteAlert, animated: true, completion: nil)
  }
  
  func enableNotification(cell: UITableViewCell) {
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      if var data = fetchedTask {
        if let notificationEnabled = data.isNotificationEnabled {
          data.isNotificationEnabled = !notificationEnabled
          if data.isNotificationEnabled! {
            self.createNotificaiton()
          } else {
            self.disableNotification()
          }
          self.dataStore.update(data, { boolean in
          })
          let indexPathRow: Int = 3
          let indexPosition = IndexPath(row: indexPathRow, section: 0)
          self.table?.reloadRows(at: [indexPosition], with: .none)
        }
      }
    }
  }
  
  func navigateToParticipantsListVC(cell: UITableViewCell) {
    let vc = ParticipantsListVC()
    vc.currentTaskID = currentTaskID
    navigationController?.pushViewController(vc, animated: true)
  }
  
  func completeTask(cell: UITableViewCell) {
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      if var data = fetchedTask {
        let isDoneTask = data.isDone
        data.isDone = !isDoneTask!
        self.dataStore.update(data, { boolean in
        })
        let indexPathRow: Int = 0
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        self.table?.reloadRows(at: [indexPosition], with: .fade)
        self.table?.reloadData()
      }
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    view.endEditing(true)
  }

  private lazy var dateFormatter: DateFormatter = {
    var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .medium
    formatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
    formatter.dateFormat = "yyyy-MM-dd hh:mm"
    return formatter
  }()
  
  @objc func tappedAwayFunction(_ sender: UITapGestureRecognizer) {
    taskInfoTextView.resignFirstResponder()
  }

  let tappedAway = UITapGestureRecognizer(target: self, action: #selector(tappedAwayFunction(_:)))
  
  lazy var scrollView: UIScrollView = {
    let view = UIScrollView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.keyboardDismissMode = .interactive
    view.alwaysBounceVertical = true
    return view
  }()
  
  private lazy var viewInsideScroll: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()

  private lazy var taskInfoTextView: UITextView = {
    let textView = UITextView()
    dataStore.fetchOne(currentTaskID, { fetchedTask in
      guard let task = fetchedTask else { return }
      textView.text = task.taskLabel
    })
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.font = UIFont.boldSystemFont(ofSize: 21)
    textView.isScrollEnabled = false
    return textView
  }()
  
  func setup() {
    setupViews()
    setupConstraints()
    setupHeader()
    setupTable()
    setupKeyboard()
  }
  
  func setupKeyboard() {
    let toolBar = UIToolbar()
    toolBar.sizeToFit()
    
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
    toolBar.setItems([flexibleSpace, doneButton], animated: false)
    
    taskInfoTextView.inputAccessoryView = toolBar
  }

  func setupHeader() {
  navigationItem.rightBarButtonItem = UIBarButtonItem(
    barButtonSystemItem: .trash,
    target: self,
    action: #selector(deleteTaskButton))
  navigationItem.title = "Details"
  }

  private func setupViews() {
    view.backgroundColor = .white
    self.view.addSubview(scrollView)
    scrollView.addSubview(viewInsideScroll)
    viewInsideScroll.addSubview(taskInfoTextView)
  }
  
  private func setupConstraints() {
    let scrollViewConstraints = [
      scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
      scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
    ]
    NSLayoutConstraint.activate(scrollViewConstraints)
    
    let viewInsideScrollConstraints = [
      viewInsideScroll.topAnchor.constraint(equalTo: scrollView.topAnchor),
      viewInsideScroll.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
      viewInsideScroll.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
      viewInsideScroll.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
      viewInsideScroll.widthAnchor.constraint(equalTo: view.widthAnchor)
    ]
    NSLayoutConstraint.activate(viewInsideScrollConstraints)
    
    let taskInfoTextViewConstraints = [
      taskInfoTextView.topAnchor.constraint(equalTo: viewInsideScroll.topAnchor, constant: 15),
      taskInfoTextView.leadingAnchor.constraint(equalTo: viewInsideScroll.leadingAnchor, constant: 15),
      taskInfoTextView.trailingAnchor.constraint(equalTo: viewInsideScroll.trailingAnchor, constant: -15)
    ]
    NSLayoutConstraint.activate(taskInfoTextViewConstraints)
  }
}

extension TaskInfoViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    constraint.constant = tableView.contentSize.height
    return 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      return getTableCellForTaskIsDone(table: tableView, row: indexPath.row)
    } else if indexPath.row == 1 {
      return getTableCellForDetails(table: tableView, row: indexPath.row)
    } else if indexPath.row == 2 {
      return getTableCellForScheduleDate(table: tableView, row: indexPath.row)
    } else if indexPath.row == 3 {
      return getTableCellForNotifications(table: tableView, row: indexPath.row)
    } else {
      return getTableCellForParticipantsList(table: tableView, row: indexPath.row)
    }
  }
  
  func getTableCellForTaskIsDone(table: UITableView, row: Int) -> UITableViewCell {
    guard let cell = table.dequeueReusableCell(withIdentifier: IsDoneCell.reuseID) as? IsDoneCell else {
      return UITableViewCell()
    }
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      let isDoneText = ((fetchedTask?.isDone)!) ? "task is done" : "task is undone"
      cell.onBind(isDoneText)
      let isDoneImage = ((fetchedTask?.isDone)!) ? UIImage(imageLiteralResourceName: "checked") : UIImage(imageLiteralResourceName: "unchecked")
      cell.onImageBind(isDoneImage)
      cell.selectionStyle = .none
      cell.link = self
    }
    return cell
  }
  
  func getTableCellForDetails(table: UITableView, row: Int) -> UITableViewCell {
    guard let cell = table.dequeueReusableCell(withIdentifier: DetailsCell.reuseID) as? DetailsCell else {
      return UITableViewCell()
    }
    cell.taskDelegate = self
    cell.link = self
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      if let detailsText = fetchedTask?.details {
        cell.onBind(detailsText)
      } else {
        cell.onBind("")
      }
    }
    cell.onImageBind(UIImage(imageLiteralResourceName: "details"))
    return cell
  }
  
  func getTableCellForScheduleDate(table: UITableView, row: Int) -> UITableViewCell {
    guard let cell = table.dequeueReusableCell(withIdentifier: ScheduleDateCell.reuseID) as? ScheduleDateCell else {
      return UITableViewCell()
    }
    cell.textDelegate = self
    cell.link = self
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      if let scheduleDate = fetchedTask?.scheduledDate {
        let formattedDate = self.dateFormatter.string(from: scheduleDate)
        cell.onBind(formattedDate)
      } else {
        cell.onBind("nil")
      }
    }
    cell.onImageBind(UIImage(imageLiteralResourceName: "schedule"))
    return cell
  }
  
  func getTableCellForNotifications(table: UITableView, row: Int) -> UITableViewCell {
    guard let cell = table.dequeueReusableCell(withIdentifier: NotificationsCell.reuseID) as? NotificationsCell else {
      return UITableViewCell()
    }
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      let notificationText = (fetchedTask?.isNotificationEnabled)! ? "notification is on" : "notification is off"
      cell.onBind(notificationText)
      let notificationImage = (fetchedTask?.isNotificationEnabled)! ?
        UIImage(imageLiteralResourceName: "alarmOn") :
        UIImage(imageLiteralResourceName: "alarmOff")
      cell.onImageBind(notificationImage)
      cell.selectionStyle = .none
      cell.link = self
    }
    return cell
  }
  
  func getTableCellForParticipantsList(table: UITableView, row: Int) -> UITableViewCell {
    guard let cell = table.dequeueReusableCell(withIdentifier: ParticipantFieldCell.reuseID) as? ParticipantFieldCell else {
      return UITableViewCell()
    }
    
    dataStore.fetchParticipants(currentTaskID) { (fetchedUsers) in
      var textArr = [String]()
      guard let participants = fetchedUsers else { return }
      for name in participants {
        textArr.append(name.name!)
        print(textArr)
      }
      cell.onBind(textArr)
    }
    cell.link = self
    cell.onImageBind(UIImage(imageLiteralResourceName: "users"))
    return cell
  }
  
}

extension TaskInfoViewController: UITextViewDelegate {
  
  func textViewDidEndEditing(_ textView: UITextView) {
    taskInfoTextView.text = textView.text
    dataStore.fetchOne(currentTaskID) { fetchedTask in
      if var taskToUpdate = fetchedTask {
        taskToUpdate.taskLabel = textView.text
        self.dataStore.update(taskToUpdate, { boolean in
        })
      }
    }
  }
}

protocol TaskInfoViewControllerProtocol {
  func returnTaskId() -> String
}

extension TaskInfoViewController: TaskInfoViewControllerProtocol {
  func returnTaskId() -> String {
    return currentTaskID
  }
}
