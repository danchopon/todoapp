//
//  ParticipantCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/28/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

class ParticipantCell: UITableViewCell {
  
  static let reuseID = "\(ParticipantCell.self)"
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
