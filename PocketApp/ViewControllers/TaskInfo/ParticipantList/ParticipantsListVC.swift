//
//  ParticipantsListVC.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/27/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

class ParticipantsListVC: UIViewController {
  
  let cellId = "cellId"
  
  let dataStore: DataStoreProtocol = FirebaseDataStore()
  var currentTaskID: String
  
  var participants = [User]()
  
  init(taskId: String) {
    self.currentTaskID = taskId
    super.init(nibName: nil, bundle: nil)
  }
  
  private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    self.currentTaskID = "\(-1)"
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    fetchParticipants()
  }
  
  private func setup() {
    setupHeader()
    setupViews()
    setupConstraints()
  }
  
  func setupHeader() {
    navigationItem.rightBarButtonItem = UIBarButtonItem(
      title: "Add",
      style: .plain, target: self,
      action: #selector(navigateToAddParticipantVC))
    navigationItem.title = "Participants"
  }
  
  private func setupViews() {
    view.addSubview(tableView)
  }
  
  private func setupConstraints() {
    let tableConstraints = [
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      tableView.topAnchor.constraint(equalTo: view.topAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ]
    NSLayoutConstraint.activate(tableConstraints)
  }
  
  func fetchParticipants() {
    dataStore.fetchParticipants(currentTaskID) { (fetchedUsers) in
      if let users = fetchedUsers {
        self.participants = users
      }
      self.tableView.reloadData()
    }
  }
  
  @objc func navigateToAddParticipantVC() {
    let vc = AddParticipantVC()
    vc.currentTaskID = currentTaskID
    let navController = UINavigationController(rootViewController: vc)
    present(navController, animated: true, completion: nil)
  }
  
  private lazy var tableView: UITableView = {
    let table = UITableView()
    table.translatesAutoresizingMaskIntoConstraints = false
    table.register(ParticipantCell.self, forCellReuseIdentifier: ParticipantCell.reuseID)
    table.dataSource = self
    table.delegate = self
    return table
  }()
}

extension ParticipantsListVC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return participants.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: ParticipantCell.reuseID, for: indexPath) as? ParticipantCell else { return UITableViewCell()}
    let user = participants[indexPath.row]
    cell.textLabel?.text = user.name
    cell.detailTextLabel?.text = user.email
    return cell
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == .delete) {
      tableView.beginUpdates()
      guard let userID = participants[indexPath.row].userID else { return }
      dataStore.deleteParticipant(userID, currentTaskID) { _ in
        
      }
      participants.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath], with: .none)
      tableView.endUpdates()
    }
  }
}
