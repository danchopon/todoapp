//
//  AddParticipantVC.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/21/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit
import Firebase

class AddParticipantVC: UIViewController {

  let cellId = "cellId"
  
  let dataStore: DataStoreProtocol = FirebaseDataStore()
  var currentTaskID: String
  
  var fetchedUsers = [User]()
  var participants = [User]()
  var allUsers = [User]()
  
  var selectedUsers = [User]()
  
  init(taskId: String) {
    self.currentTaskID = taskId
    super.init(nibName: nil, bundle: nil)
  }
  
  private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    self.currentTaskID = "\(-1)"
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    fetchParticipants()
  }
  
  private func setup() {
    setupHeader()
    setupViews()
    setupConstraints()
  }
  
  func setupHeader() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(
      title: "Cancel",
      style: .plain, target: self,
      action: #selector(handleCancel))
    navigationItem.rightBarButtonItem = UIBarButtonItem(
      title: "Done",
      style: .plain, target: self,
      action: #selector(saveSelectedUsers))
    navigationItem.rightBarButtonItem?.isEnabled = !selectedUsers.isEmpty
    navigationItem.title = "Add users"
  }
  
  private func setupViews() {
    view.addSubview(tableView)
  }
  
  private func setupConstraints() {
    let tableConstraints = [
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      tableView.topAnchor.constraint(equalTo: view.topAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ]
    NSLayoutConstraint.activate(tableConstraints)
  }
  
  func fetchParticipants() {
    dataStore.fetchParticipants(currentTaskID) { (fUsers) in
      if let users = fUsers {
        self.participants = users
      }
      self.fetchAllUsers()
    }
  }
  
  func fetchAllUsers() {
    dataStore.fetchAllUsers { (users) in
      if let fetchedUsers = users {
        self.fetchedUsers = fetchedUsers
      }
      self.allUsers = self.fetchedUsers.difference(from: self.participants)
      self.tableView.reloadData()
    }
  }
  
  @objc func saveSelectedUsers() {
    dataStore.saveUserInTask(selectedUsers, currentTaskID) { _ in
    }
    dismiss(animated: true, completion: nil)
  }
  
  @objc func handleCancel() {
    dismiss(animated: true, completion: nil)
  }
  
  private lazy var tableView: UITableView = {
    let table = UITableView()
    table.translatesAutoresizingMaskIntoConstraints = false
    table.register(AddUserCell.self, forCellReuseIdentifier: AddUserCell.reuseID)
    table.dataSource = self
    table.delegate = self
    table.allowsMultipleSelection = true
    return table
  }()
}

extension AddParticipantVC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return allUsers.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let cell = tableView.dequeueReusableCell(withIdentifier: AddUserCell.reuseID, for: indexPath) as? AddUserCell else { return UITableViewCell() }
    let user = allUsers[indexPath.row]
    cell.textLabel?.text = user.name
    cell.detailTextLabel?.text = user.email
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedUsers.append(allUsers[indexPath.row])
    navigationItem.rightBarButtonItem?.isEnabled = !selectedUsers.isEmpty
  }
  
  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let index = selectedUsers.index(of: allUsers[indexPath.row]) {
      selectedUsers.remove(at: index)
    }
    navigationItem.rightBarButtonItem?.isEnabled = !selectedUsers.isEmpty
  }
}

extension Array where Element: Hashable {
  func difference(from other: [Element]) -> [Element] {
    let thisSet = Set(self)
    let otherSet = Set(other)
    return Array(thisSet.symmetricDifference(otherSet))
  }
}
