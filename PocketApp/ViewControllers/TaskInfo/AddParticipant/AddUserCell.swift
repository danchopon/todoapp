//
//  AddUserCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/28/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

class AddUserCell: UITableViewCell {
  
  static let reuseID = "\(AddUserCell.self)"
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    accessoryType = selected ? .checkmark : .none
  }
}
