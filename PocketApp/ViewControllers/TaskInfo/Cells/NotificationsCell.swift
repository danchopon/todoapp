//
//  NotificationsCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 12/19/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {
  
  static let reuseID = "\(NotificationsCell.self)"
  
  var link: TaskInfoViewController?
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init coder has not been implemented")
  }
  
  @objc private func handleTaskNotifications() {
    print("\(#function)")
    link?.enableNotification(cell: self)
  }
  
  private lazy var leftView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var rightView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var notificationLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.systemFont(ofSize: 15)
//    label.backgroundColor = UIColor.red
    return label
  }()
  
  private lazy var doneButton: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(handleTaskNotifications), for: .touchUpInside)
    return button
  }()
  
  func onBind(_ text: String) {
    notificationLabel.text = text
  }
  
  func onImageBind(_ image: UIImage) {
    doneButton.setImage(image, for: .normal)
  }
  
  private func setup() {
    setupViews()
    setupConstraints()
  }
  
  private func setupViews() {
    contentView.addSubview(leftView)
    contentView.addSubview(rightView)
    leftView.addSubview(doneButton)
    rightView.addSubview(notificationLabel)
  }
  
  private func setupConstraints() {
    
    let leftViewC = [
      leftView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      leftView.topAnchor.constraint(equalTo: contentView.topAnchor),
      leftView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      leftView.widthAnchor.constraint(equalToConstant: 50),
      leftView.heightAnchor.constraint(equalToConstant: 50)
    ]
    NSLayoutConstraint.activate(leftViewC)
    
    let rightViewC = [
      rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor),
      rightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      rightView.topAnchor.constraint(equalTo: contentView.topAnchor),
      rightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    NSLayoutConstraint.activate(rightViewC)
    
    let doneButtonC = [
      doneButton.topAnchor.constraint(equalTo: leftView.topAnchor),
      doneButton.centerXAnchor.constraint(equalTo: leftView.centerXAnchor),
      doneButton.centerYAnchor.constraint(equalTo: leftView.centerYAnchor)
    ]
    NSLayoutConstraint.activate(doneButtonC)
    
    let notificationLabelC = [
      notificationLabel.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      notificationLabel.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      notificationLabel.topAnchor.constraint(equalTo: rightView.topAnchor, constant: 0),
      notificationLabel.bottomAnchor.constraint(equalTo: rightView.bottomAnchor, constant: 0)
    ]
    NSLayoutConstraint.activate(notificationLabelC)
  }
}
