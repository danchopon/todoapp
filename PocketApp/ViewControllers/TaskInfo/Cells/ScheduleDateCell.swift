//
//  ScheduleDateCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 12/19/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

protocol DatePickerDelegate: class {
  func didChangeDate(date: Date)
}

class ScheduleDateCell: UITableViewCell {

  static let reuseID = "\(ScheduleDateCell.self)"
  
  var link: TaskInfoViewController?
  var textDelegate: TaskInfoViewControllerProtocol?

  let dataStore: DataStoreProtocol = FirebaseDataStore()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init coder has not been implemented")
  }
  
  weak var delegate: DatePickerDelegate?
  
  func updateText(date: Date) {
    scheduleTextView.text = dateFormatter.string(from: date)
  }

  @objc private func dismissKeyboard() {
    link!.dismissCellKeyboard(cell: self)
  }
  
  @objc func dateDidChange(_ sender: UIDatePicker) {
    delegate?.didChangeDate(date: sender.date)
  }
  
  private lazy var leftView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var rightView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var doneButton: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  private lazy var scheduleTextView: UITextView = {
    let textView = UITextView()
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.font = UIFont.systemFont(ofSize: 15)
    textView.textContainer.lineFragmentPadding = 0
    textView.textContainerInset = .zero
    return textView
  }()
  
  private lazy var datePicker: UIDatePicker = {
    let picker =  UIDatePicker()
    picker.datePickerMode = .dateAndTime
    picker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
    picker.minuteInterval = 5
    return picker
  }()
  
  private lazy var dateFormatter: DateFormatter = {
    var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .medium
    formatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
    formatter.dateFormat = "yyyy-MM-dd hh:mm"
    return formatter
  }()
  
  @objc func datePickerChanged(_ sender: UIDatePicker) {
    let id = textDelegate?.returnTaskId()
    scheduleTextView.text = dateFormatter.string(from: sender.date)
    
    dataStore.fetchOne(id!) { fetchedTask in
      if var taskToUpdate = fetchedTask {
        taskToUpdate.scheduledDate = sender.date
        self.dataStore.update(taskToUpdate, { boolean in
        })
      }
    }
  }

  func onBind(_ text: String) {
    if text == "nil" {
      scheduleTextView.text = "Set date"
    } else {
      scheduleTextView.text = text
    }
  }
  
  func onImageBind(_ image: UIImage) {
    doneButton.setImage(image, for: .normal)
  }
  
  private func setup() {
    setupViews()
    setupConstraints()
    setupKeyboard()
    datePicker.addTarget(self, action: #selector(dateDidChange), for: .valueChanged)
    scheduleTextView.inputView = datePicker
  }
  
  private func setupKeyboard() {
    let toolBar = UIToolbar()
    toolBar.sizeToFit()
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    toolBar.setItems([flexibleSpace, doneButton], animated: false)
    scheduleTextView.inputAccessoryView = toolBar
  }
  
  private func setupViews() {
    contentView.addSubview(leftView)
    contentView.addSubview(rightView)
    leftView.addSubview(doneButton)
    rightView.addSubview(scheduleTextView)
  }
  
  private func setupConstraints() {
    let leftViewC = [
      leftView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      leftView.topAnchor.constraint(equalTo: contentView.topAnchor),
      leftView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      leftView.widthAnchor.constraint(equalToConstant: 50),
      leftView.heightAnchor.constraint(equalToConstant: 50)
    ]
    NSLayoutConstraint.activate(leftViewC)
    
    let rightViewC = [
      rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor),
      rightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      rightView.topAnchor.constraint(equalTo: contentView.topAnchor),
      rightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    NSLayoutConstraint.activate(rightViewC)
    
    let doneButtonC = [
      doneButton.topAnchor.constraint(equalTo: leftView.topAnchor),
      doneButton.centerXAnchor.constraint(equalTo: leftView.centerXAnchor),
      doneButton.centerYAnchor.constraint(equalTo: leftView.centerYAnchor)
    ]
    NSLayoutConstraint.activate(doneButtonC)
    
    let scheduleTextViewC = [
      scheduleTextView.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      scheduleTextView.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      scheduleTextView.topAnchor.constraint(equalTo: rightView.topAnchor, constant: 0),
      scheduleTextView.bottomAnchor.constraint(equalTo: rightView.bottomAnchor, constant: 0)
    ]
    NSLayoutConstraint.activate(scheduleTextViewC)
  }
}
