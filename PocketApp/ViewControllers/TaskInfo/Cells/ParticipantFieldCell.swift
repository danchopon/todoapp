//
//  AddParticipantCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/21/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

class ParticipantFieldCell: UITableViewCell {
  static let reuseID = "\(ParticipantFieldCell.self)"
  
  var link: TaskInfoViewController?
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init coder has not been implemented")
  }
  
  @objc private func navigateToAddParticipantVC() {
    print("\(#function)")
    link?.navigateToParticipantsListVC(cell: self)
  }
  
  private lazy var leftView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var rightView: UIView = {
    let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToAddParticipantVC))
    gesture.numberOfTapsRequired = 1
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.isUserInteractionEnabled = true
    view.addGestureRecognizer(gesture)
    return view
  }()
  
  private lazy var label: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.systemFont(ofSize: 15)
    return label
  }()
  
  private lazy var button: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  func onBind(_ textArr: [String]) {
    
    if !textArr.isEmpty {
      label.text = textArr.joined(separator: ", ")
    } else {
      label.text = "Add participant"
      label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight(300))
      label.textColor = UIColor.blue
    }
   
  }
  
  func onImageBind(_ image: UIImage) {
    button.setImage(image, for: .normal)
  }
  
  private func setup() {
    setupViews()
    setupConstraints()
  }
  
  private func setupViews() {
    contentView.addSubview(leftView)
    contentView.addSubview(rightView)
    leftView.addSubview(button)
    rightView.addSubview(label)
  }
  
  private func setupConstraints() {
    
    let leftViewC = [
      leftView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      leftView.topAnchor.constraint(equalTo: contentView.topAnchor),
      leftView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      leftView.widthAnchor.constraint(equalToConstant: 50),
      leftView.heightAnchor.constraint(equalToConstant: 50)
    ]
    NSLayoutConstraint.activate(leftViewC)
    
    let rightViewC = [
      rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor),
      rightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      rightView.topAnchor.constraint(equalTo: contentView.topAnchor),
      rightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    NSLayoutConstraint.activate(rightViewC)
    
    let doneButtonC = [
      button.topAnchor.constraint(equalTo: leftView.topAnchor),
      button.centerXAnchor.constraint(equalTo: leftView.centerXAnchor),
      button.centerYAnchor.constraint(equalTo: leftView.centerYAnchor)
    ]
    NSLayoutConstraint.activate(doneButtonC)
    
    let notificationLabelC = [
      label.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      label.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      label.topAnchor.constraint(equalTo: rightView.topAnchor, constant: 0),
      label.bottomAnchor.constraint(equalTo: rightView.bottomAnchor, constant: 0)
    ]
    NSLayoutConstraint.activate(notificationLabelC)
  }
}
