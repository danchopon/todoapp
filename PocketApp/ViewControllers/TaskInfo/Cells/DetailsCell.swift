//
//  TaskInfoCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 12/13/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

extension UITableViewCell {
  /// Search up the view hierarchy of the table view cell to find the containing table view
  var tableView: UITableView? {
    get {
      var table: UIView? = superview
      while !(table is UITableView) && table != nil {
        table = table?.superview
      }
      return table as? UITableView
    }
  }
}

extension DetailsCell: UITextViewDelegate {

  func textViewDidChange(_ textView: UITextView) {
    let size = textView.bounds.size
    let newSize = textView.sizeThatFits(CGSize(width:size.width, height: CGFloat.greatestFiniteMagnitude))
    
    // Resize the cell only when cell's size is changed
    if size.height != newSize.height {
      UIView.setAnimationsEnabled(false)
      tableView?.beginUpdates()
      tableView?.endUpdates()
      UIView.setAnimationsEnabled(true)
      if let thisIndexPath = tableView?.indexPath(for: self) {
        tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
      }
    }
  }
  
  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    detailsTextView = textView
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    let taskID = taskDelegate?.returnTaskId()
    detailsTextView.text = textView.text
    
    dataStore.fetchOne(taskID!) { fetchedTask in
      guard var taskToUpdate = fetchedTask else { return }
      taskToUpdate.details = textView.text
      self.dataStore.update(taskToUpdate, { boolean in
      })
    }
    tableView?.reloadData()
  }
}

class DetailsCell: UITableViewCell {

  let dataStore: DataStoreProtocol = FirebaseDataStore()
  var taskDelegate: TaskInfoViewControllerProtocol?
  static let reuseID = "\(DetailsCell.self)"
  var link: TaskInfoViewController?
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillShow),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillHide),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillChange),
                                           name: UIResponder.keyboardWillChangeFrameNotification,
                                           object: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init coder has not been implemented")
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    var userInfo = notification.userInfo!
    if let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//      // Get my height size
//      let myheight = link?.scrollView.frame.height
//      // Get the top Y point where the keyboard will finish on the view
//      let keyboardEndPoint = myheight! - keyboardFrame.height
//      // Get the the bottom Y point of the textInput and transform it to the currentView coordinates.
//      if let pointInTable = detailsTextView.superview?.convert(detailsTextView.frame.origin, to: link?.scrollView) {
//        let textFieldBottomPoint = pointInTable.y + detailsTextView.frame.size.height + 20
//        // Finally check if the keyboard will cover the textInput
//        if keyboardEndPoint <= textFieldBottomPoint {
//          link?.scrollView.contentOffset.y = textFieldBottomPoint - keyboardEndPoint
//        } else {
//          link?.scrollView.contentOffset.y = 0
//        }
//      }
      link?.scrollView.contentInset.bottom = (link?.view.convert(keyboardFrame, from: nil).size.height)!
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    tableView!.contentOffset.y = 0
  }
  
  var keyboardHeightLayoutConstraint: NSLayoutConstraint?
  @objc func keyboardWillChange(notification: NSNotification) {
    if let userInfo = notification.userInfo {
      let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
      let endFrameY = endFrame!.origin.y ?? 0
      let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
      let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
      let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
      let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
      if endFrameY >= UIScreen.main.bounds.size.height {
        self.keyboardHeightLayoutConstraint?.constant = 0.0
      } else {
        self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
      }
      UIView.animate(withDuration: duration,
                     delay: TimeInterval(0),
                     options: animationCurve,
                     animations: { self.link?.view.layoutIfNeeded() },
                     completion: nil)
    }
  }
  
  private lazy var leftView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var rightView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var doneButton: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  
  private lazy var detailsTextView: UITextView = {
    let textView = UITextView()
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.font = UIFont.systemFont(ofSize: 17)
    textView.isScrollEnabled = false
    textView.delegate = self
    textView.textContainer.lineFragmentPadding = 0
    textView.textContainerInset = .zero
    return textView
  }()
  
  var textString: String {
    get {
      return detailsTextView.text
    }
    set {
      detailsTextView.text = newValue
      textViewDidChange(detailsTextView)
    }
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    print("root selected")
    if selected {
      print("selected ! ! ! !")
      detailsTextView.becomeFirstResponder()
    } else {
      print("UNselected ! ! ! !")
      detailsTextView.resignFirstResponder()
    }
  }
  
  func onBind(_ text: String) {
    detailsTextView.text = text
  }
  
  func onImageBind(_ image: UIImage) {
    doneButton.setImage(image, for: .normal)
  }

  private func setup() {
    setupViews()
    setupConstraints()
    setupKeyboard()
  }
  
  private func setupKeyboard() {
    let toolBar = UIToolbar()
    toolBar.sizeToFit()
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    toolBar.setItems([flexibleSpace, doneButton], animated: false)
    
    detailsTextView.inputAccessoryView = toolBar
  }
  
  @objc private func dismissKeyboard() {
    link!.dismissCellKeyboard(cell: self)
  }
  
  private func setupViews() {
    contentView.addSubview(leftView)
    contentView.addSubview(rightView)
    leftView.addSubview(doneButton)
    rightView.addSubview(detailsTextView)
  }
  
  private func setupConstraints() {
    let leftViewC = [
      leftView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      leftView.topAnchor.constraint(equalTo: contentView.topAnchor),
      leftView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -25),
      leftView.widthAnchor.constraint(equalToConstant: 50)
    ]
    NSLayoutConstraint.activate(leftViewC)
    
    let rightViewC = [
      rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor),
      rightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      rightView.topAnchor.constraint(equalTo: contentView.topAnchor),
      rightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -25)
    ]
    NSLayoutConstraint.activate(rightViewC)
    
    let doneButtonC = [
      doneButton.topAnchor.constraint(equalTo: leftView.topAnchor),
      doneButton.centerXAnchor.constraint(equalTo: leftView.centerXAnchor)
    ]
    NSLayoutConstraint.activate(doneButtonC)
    
    let taskLabelC = [
      detailsTextView.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      detailsTextView.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      detailsTextView.topAnchor.constraint(equalTo: rightView.topAnchor, constant: 0),
      detailsTextView.bottomAnchor.constraint(equalTo: rightView.bottomAnchor, constant: 0)
    ]
    NSLayoutConstraint.activate(taskLabelC)
  }
}
