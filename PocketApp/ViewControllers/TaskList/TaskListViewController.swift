//
//  TaskListViewController.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/22/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit
import Firebase

class TaskListViewController: UIViewController {

  let dataStore: DataStoreProtocol = FirebaseDataStore()
  let defaults = UserDefaults.standard
  let keyForUserDefaults = "myKey"
  let cellId = "cellId"
  var taskList = [Task]()
  var sharedTasks = [Task]()
  
  lazy var rowsToDisplay = taskList
  
  var taskTextField = UITextField()
  
  private lazy var tableView: UITableView = {
    let table = UITableView()
    table.translatesAutoresizingMaskIntoConstraints = false
    table.register(TaskCell.self, forCellReuseIdentifier: cellId)
    table.dataSource = self
    table.delegate = self
    table.rowHeight = 50
    return table
  }()
  
  lazy var segmentedControl: UISegmentedControl = {
    let sc = UISegmentedControl(items: ["My tasks", "Shared tasks"])
    sc.selectedSegmentIndex = 0
    sc.addTarget(self, action: #selector(handleSegmentChange), for: .valueChanged)
    return sc
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    checkIfUserIsLoggedIn()
    
    setupViews()
    setupConstraints()
    setupHeader()
    
    dataStore.fetchAll { fetchedTasks in
      if let taskList = fetchedTasks {
        self.taskList = taskList
        self.tableView.reloadData()
      } else {
        print("nil")
      }
    }
    
    dataStore.fetchUsersParticipationTasks { fetchedTask in
      if let sharedTasks = fetchedTask {
        self.sharedTasks = sharedTasks
        self.tableView.reloadData()
      } else {
        print("nil")
      }
    }
  }
  
  func checkIfUserIsLoggedIn() {
    if Auth.auth().currentUser?.uid == nil {
      perform(#selector(handleLogout), with: nil, afterDelay: 0)
    }
  }
  
  @objc fileprivate func handleSegmentChange() {
    print(segmentedControl.selectedSegmentIndex)
    switch segmentedControl.selectedSegmentIndex {
    case 0:
      rowsToDisplay = taskList
      print(rowsToDisplay)
    default:
      rowsToDisplay = sharedTasks
      print(rowsToDisplay)
    }
    tableView.reloadData()
  }
  
  func addNewTask() {
    if self.taskTextField.text != "" {
      let task = Task(taskID: nil,
                      taskLabel: self.taskTextField.text!,
                      isDone: false,
                      creationDate: Date(),
                      scheduledDate: nil,
                      isNotificationEnabled: false,
                      details: nil)
      self.dataStore.save(task) { boolean in
      }
      self.taskList.append(task)
      self.tableView.reloadData()
    }
  }

  @objc func addTaskButton() {
    let alert = UIAlertController(title: "Add New Task", message: "", preferredStyle: .alert)
    let action = UIAlertAction(title: "Add Task", style: .default) { (action) in
      self.addNewTask()
      self.tableView.reloadData()
    }
    alert.addTextField { (alertTextField) in
      alertTextField.placeholder = "Add new task"
      self.taskTextField = alertTextField
    }
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }

  func doneTaskButton(cell: UITableViewCell) {
    guard let indexPathTapped = tableView.indexPath(for: cell) else { return }
    let taskItem = taskList[indexPathTapped.row]
    if let hasDone = taskItem.isDone {
      taskList[indexPathTapped.row].isDone = !hasDone
      dataStore.update(taskList[indexPathTapped.row]) { boolean in
        cell.accessoryView?.tintColor = hasDone ? UIColor.lightGray : .red
      }
      tableView.reloadRows(at: [indexPathTapped], with: .fade)
    }
  }
  
  @objc func handleLogout() {
    do {
      try Auth.auth().signOut()
    } catch let logoutError {
      print(logoutError)
    }
    
    let loginVC = LoginViewController()
    navigationController?.pushViewController(loginVC, animated: true)
  }

  func setupHeader() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
    navigationItem.rightBarButtonItem = UIBarButtonItem(
      barButtonSystemItem: .add,
      target: self,
      action: #selector(addTaskButton))
    navigationItem.titleView = segmentedControl
  }

  private func setupViews() {
    view.addSubview(tableView)
  }
  
  private func setupConstraints() {
    let tableConstraints = [
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      tableView.topAnchor.constraint(equalTo: view.topAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ]
    NSLayoutConstraint.activate(tableConstraints)
  }
}

extension TaskListViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return rowsToDisplay.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let taskItem = rowsToDisplay[indexPath.row]
    let formatter = DateFormatter()
    formatter.dateStyle = .medium

    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TaskCell else { return UITableViewCell() }
    cell.link = self
    if let label = taskItem.taskLabel {
      cell.onBind(label)
    }
    if let schedDate = taskItem.scheduledDate {
      let scheduledDate = formatter.string(from: (schedDate))
      cell.onDateBind(scheduledDate)
    } else {
      cell.onDateBind("")
    }
    if let isDone = taskItem.isDone {
      let isDoneImage = isDone ? UIImage(imageLiteralResourceName: "checked") : UIImage(imageLiteralResourceName: "unchecked")
      cell.onImageBind(isDoneImage)
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let detailsVC = TaskInfoViewController()
    detailsVC.currentTaskID = rowsToDisplay[indexPath.row].taskID!
    
    self.navigationController?.pushViewController(detailsVC, animated: true)
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == .delete) {
      tableView.beginUpdates()
      
      let taskItem = rowsToDisplay[indexPath.row]
      guard let taskToDelete = taskItem.taskID else { return }
      dataStore.deleteOne(taskToDelete) { boolean in
        
      }
      rowsToDisplay.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath], with: .none)
      tableView.endUpdates()
    }
  }
}
