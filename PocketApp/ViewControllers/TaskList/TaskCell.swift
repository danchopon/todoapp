//
//  TaskCell.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/28/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    
  var link: TaskListViewController?
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init coder has not been implemented")
  }
  
  @objc private func handleTaskAsDone() {
    link?.doneTaskButton(cell: self)
  }
  
  private lazy var leftView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var rightView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var taskLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.boldSystemFont(ofSize: 18)
    return label
  }()
  
  private lazy var taskScheduleDateLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.systemFont(ofSize: 12)
    return label
  }()
  
  private lazy var doneButton: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setImage(#imageLiteral(resourceName: "fav_star"), for: .normal)
    button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    button.addTarget(self, action: #selector(handleTaskAsDone), for: .touchUpInside)
    return button
  }()
  
  func onBind(_ text: String) {
    taskLabel.text = text
  }
  
  func onDateBind(_ dateText: String) {
    taskScheduleDateLabel.text = dateText
  }
  
  func onImageBind(_ image: UIImage) {
    doneButton.setImage(image, for: .normal)
  }
      
  private func setup() {
    setupViews()
    setupConstraints()
  }
  
  private func setupViews() {
    contentView.addSubview(leftView)
    contentView.addSubview(rightView)
    leftView.addSubview(doneButton)
    rightView.addSubview(taskLabel)
    rightView.addSubview(taskScheduleDateLabel)
  }
  
  private func setupConstraints() {
    
    let leftViewC = [
      leftView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      leftView.topAnchor.constraint(equalTo: contentView.topAnchor),
      leftView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      leftView.widthAnchor.constraint(equalToConstant: 50)
      ]
    NSLayoutConstraint.activate(leftViewC)
    
    let rightViewC = [
      rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor),
      rightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      rightView.topAnchor.constraint(equalTo: contentView.topAnchor),
      rightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    NSLayoutConstraint.activate(rightViewC)
    
    let doneButtonC = [
      doneButton.topAnchor.constraint(equalTo: leftView.topAnchor, constant: 8.0),
      doneButton.centerXAnchor.constraint(equalTo: leftView.centerXAnchor)
    ]
    NSLayoutConstraint.activate(doneButtonC)
    
    let taskLabelC = [
      taskLabel.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      taskLabel.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      taskLabel.topAnchor.constraint(equalTo: rightView.topAnchor, constant: 8.0)
    ]
    NSLayoutConstraint.activate(taskLabelC)
    
    let taskScheduleDateLabelC = [
      taskScheduleDateLabel.leadingAnchor.constraint(equalTo: rightView.leadingAnchor, constant: 8.0),
      taskScheduleDateLabel.trailingAnchor.constraint(equalTo: rightView.trailingAnchor, constant: -8.0),
      taskScheduleDateLabel.topAnchor.constraint(equalTo: taskLabel.bottomAnchor, constant: 4.0),
      taskScheduleDateLabel.bottomAnchor.constraint(equalTo: rightView.bottomAnchor, constant: -4.0)
    ]
    NSLayoutConstraint.activate(taskScheduleDateLabelC)
    
  }
}
