//
//  LoginViewController+handlers.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/19/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

extension LoginViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  @objc func handleSelectProfileImageView() {
    let picker = UIImagePickerController()
    
    picker.delegate = self
    picker.allowsEditing = true
    
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    var selectedImageFromPicker: UIImage?
    
    if let editedImage = info[.editedImage] as? UIImage {
      selectedImageFromPicker = editedImage
    } else if let originalImage = info[.originalImage] as? UIImage {
      selectedImageFromPicker = originalImage
    }
    
    if let selectedImage = selectedImageFromPicker {
      profileImageView.image = selectedImage
    }
    
    dismiss(animated: true, completion: nil)
    
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }
  
}
