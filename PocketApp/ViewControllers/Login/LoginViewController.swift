//
//  LoginController.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/18/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
  
  let dataStore: DataStoreProtocol = FirebaseDataStore()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
      if self.view.frame.origin.y == 0 {
        self.view.frame.origin.y -= keyboardSize.height - 100
      }
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    if self.view.frame.origin.y != 0 {
      self.view.frame.origin.y = 0
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
  }
  
  lazy var scrollView: UIScrollView = {
    let view = UIScrollView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.keyboardDismissMode = .interactive
    view.alwaysBounceVertical = true
    return view
  }()
  
  lazy var viewInsideScroll: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var inputsContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.white
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layer.cornerRadius = 5
    view.layer.masksToBounds = true
    return view
  }()
  
  lazy var loginRegisterButton: UIButton = {
    let button = UIButton(type: .system)
    button.backgroundColor = UIColor(red: 80, green: 101, blue: 161)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle("Register", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
    button.layer.cornerRadius = 5
    button.layer.masksToBounds = true
    button.addTarget(self, action: #selector(handleLoginRegister), for: .touchUpInside)
    return button
  }()
  
  lazy var loginSpinner: UIActivityIndicatorView = {
    let spinner = UIActivityIndicatorView(style: .white)
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.hidesWhenStopped = true
    return spinner
  }()
  
  @objc func handleLoginRegister() {
    guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text, let image = profileImageView.image?.pngData() else {
      print("Form is not valid")
      return
    }
    
    if loginRegisterSegmentedControl.selectedSegmentIndex == 0 {
      handleLogin(email, password)
    } else {
      handleRegister(email, password, name, image)
    }
  }
  
  @objc func handleLogin(_ email: String, _ password: String) {
    loginSpinner.startAnimating()
    loginRegisterButton.setTitle("", for: .disabled)
    dataStore.loginUser(email, password) { (boolean) in
      self.navigateToTaskListVC()
    }
    loginSpinner.stopAnimating()
  }
 
  @objc func handleRegister(_ email: String, _ password: String, _ name: String, _ image: Data) {
    dataStore.registerUser(name, email, password, image) { boolean in
      self.navigateToTaskListVC()
    }
  }
  
  func navigateToTaskListVC() {
    let listVC = TaskListViewController()
    self.navigationController?.pushViewController(listVC, animated: true)
  }
  
  lazy var nameTextField: UITextField = {
    let textF = UITextField()
    textF.translatesAutoresizingMaskIntoConstraints = false
    textF.placeholder = "Name"
    return textF
  }()
  
  lazy var nameSeparatorView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor(red: 220, green: 220, blue: 220)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var emailTextField: UITextField = {
    let textF = UITextField()
    textF.translatesAutoresizingMaskIntoConstraints = false
    textF.placeholder = "Email"
    return textF
  }()
  
  lazy var emailSeparatorView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor(red: 220, green: 220, blue: 220)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var passwordTextField: UITextField = {
    let textF = UITextField()
    textF.translatesAutoresizingMaskIntoConstraints = false
    textF.placeholder = "Password"
    textF.isSecureTextEntry = true
    return textF
  }()
  
  lazy var profileImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.image = UIImage(named: "fav_star")
    imageView.contentMode = .scaleAspectFill
    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
    imageView.isUserInteractionEnabled = true
    return imageView
  }()
  
  lazy var loginRegisterSegmentedControl: UISegmentedControl = {
    let sc = UISegmentedControl(items: ["Login", "Register"])
    sc.translatesAutoresizingMaskIntoConstraints = false
    sc.tintColor = UIColor.white
    sc.selectedSegmentIndex = 1
    sc.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
    return sc
  }()
  
  @objc func handleLoginRegisterChange() {
    let title = loginRegisterSegmentedControl.titleForSegment(at: loginRegisterSegmentedControl.selectedSegmentIndex)
    loginRegisterButton.setTitle(title, for: .normal)
    
    inputsContainerViewHeightAnchor!.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 100 : 150
    nameTextFieldHeightAnchor?.isActive = false
    nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/3)
    nameTextFieldHeightAnchor?.isActive = true
    
    emailTextFieldHeightAnchor?.isActive = false
    emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
    emailTextFieldHeightAnchor?.isActive = true
    
    passwordTextFieldHeightAnchor?.isActive = false
    passwordTextFieldHeightAnchor=passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
    passwordTextFieldHeightAnchor?.isActive = true
    
  }
  
  var inputsContainerViewHeightAnchor: NSLayoutConstraint?
  var nameTextFieldHeightAnchor: NSLayoutConstraint?
  var emailTextFieldHeightAnchor: NSLayoutConstraint?
  var passwordTextFieldHeightAnchor: NSLayoutConstraint?
  
  private func setup() {
    setupViews()
    setupConstraints()
  }
  
  private func setupViews() {
    view.backgroundColor = UIColor(red: 61, green: 91, blue: 151)
    view.addSubview(scrollView)
    scrollView.addSubview(viewInsideScroll)
    viewInsideScroll.addSubview(profileImageView)
    viewInsideScroll.addSubview(loginRegisterSegmentedControl)
    viewInsideScroll.addSubview(inputsContainerView)
    inputsContainerView.addSubview(nameTextField)
    inputsContainerView.addSubview(nameSeparatorView)
    inputsContainerView.addSubview(emailTextField)
    inputsContainerView.addSubview(emailSeparatorView)
    inputsContainerView.addSubview(passwordTextField)
    viewInsideScroll.addSubview(loginRegisterButton)
    loginRegisterButton.addSubview(loginSpinner)
  }
  
  private func setupConstraints() {
    let scrollViewConstraints = [
      scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
      scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
    ]
    NSLayoutConstraint.activate(scrollViewConstraints)
    
    let viewInsideScrollConstraints = [
      viewInsideScroll.topAnchor.constraint(equalTo: scrollView.topAnchor),
      viewInsideScroll.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
      viewInsideScroll.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
      viewInsideScroll.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
      viewInsideScroll.widthAnchor.constraint(equalTo: view.widthAnchor)
    ]
    NSLayoutConstraint.activate(viewInsideScrollConstraints)
    
    inputsContainerViewHeightAnchor = inputsContainerView.heightAnchor.constraint(equalToConstant: 150)
    inputsContainerViewHeightAnchor?.isActive = true
    
    let inputsConstraints = [
      inputsContainerView.centerXAnchor.constraint(equalTo: viewInsideScroll.centerXAnchor),
      inputsContainerView.centerYAnchor.constraint(equalTo: viewInsideScroll.centerYAnchor),
      viewInsideScroll.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor, constant: 24)
    ]
    NSLayoutConstraint.activate(inputsConstraints)
    
    let loginButtonC = [
      loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      loginRegisterButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 12),
      loginRegisterButton.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor),
      loginRegisterButton.heightAnchor.constraint(equalToConstant: 30),
      loginRegisterButton.bottomAnchor.constraint(equalTo: loginRegisterButton.bottomAnchor)
    ]
    NSLayoutConstraint.activate(loginButtonC)
    
    nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
    nameTextFieldHeightAnchor?.isActive = true
    
    let nameTextFieldC = [
      nameTextField.leadingAnchor.constraint(equalTo: inputsContainerView.leadingAnchor, constant: 12),
      nameTextField.topAnchor.constraint(equalTo: inputsContainerView.topAnchor),
      nameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor)
    ]
    NSLayoutConstraint.activate(nameTextFieldC)
    
    let nameSeparatorViewC = [
      nameSeparatorView.leadingAnchor.constraint(equalTo: inputsContainerView.leadingAnchor),
      nameSeparatorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor),
      nameSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor),
      nameSeparatorView.heightAnchor.constraint(equalToConstant: 1)
    ]
    NSLayoutConstraint.activate(nameSeparatorViewC)
    
    emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
    emailTextFieldHeightAnchor?.isActive = true
    
    let emailTextFieldC = [
      emailTextField.leadingAnchor.constraint(equalTo: inputsContainerView.leadingAnchor, constant: 12),
      emailTextField.topAnchor.constraint(equalTo: nameSeparatorView.bottomAnchor),
      emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor)
    ]
    NSLayoutConstraint.activate(emailTextFieldC)

    let emailSeparatorViewC = [
      emailSeparatorView.leadingAnchor.constraint(equalTo: inputsContainerView.leadingAnchor),
      emailSeparatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor),
      emailSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor),
      emailSeparatorView.heightAnchor.constraint(equalToConstant: 1)
    ]
    NSLayoutConstraint.activate(emailSeparatorViewC)
    
    passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
    passwordTextFieldHeightAnchor?.isActive = true
    
    let passwordTextFieldC = [
      passwordTextField.leadingAnchor.constraint(equalTo: inputsContainerView.leadingAnchor, constant: 12),
      passwordTextField.topAnchor.constraint(equalTo: emailSeparatorView.bottomAnchor),
      passwordTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor)
    ]
    NSLayoutConstraint.activate(passwordTextFieldC)

    let profileImageViewC = [
      profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      profileImageView.topAnchor.constraint(equalTo: viewInsideScroll.topAnchor, constant: 12),
      loginRegisterSegmentedControl.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 12),
      profileImageView.widthAnchor.constraint(equalToConstant: 150),
      profileImageView.heightAnchor.constraint(equalToConstant: 150)
    ]
    NSLayoutConstraint.activate(profileImageViewC)
    
    let loginRegisterSegmentedControlConstraints = [
      loginRegisterSegmentedControl.centerXAnchor.constraint(equalTo: viewInsideScroll.centerXAnchor),
      inputsContainerView.topAnchor.constraint(equalTo: loginRegisterSegmentedControl.bottomAnchor, constant: 12),
      loginRegisterSegmentedControl.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor, multiplier: 1),
      loginRegisterSegmentedControl.heightAnchor.constraint(equalToConstant: 36)
    ]
    NSLayoutConstraint.activate(loginRegisterSegmentedControlConstraints)
    
    let loginSpinnerC = [
      loginSpinner.centerXAnchor.constraint(equalTo: loginRegisterButton.centerXAnchor),
      loginSpinner.centerYAnchor.constraint(equalTo: loginRegisterButton.centerYAnchor)
    ]
    NSLayoutConstraint.activate(loginSpinnerC)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}

extension UIColor {
  convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
    self.init(red: red/255, green: red/255, blue: blue/255, alpha: 1)
  }
}
