//
//  NetworkDataStore.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 12/26/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

class NetworkDataStore: DataStoreProtocol {
  
  func fetchUsersParticipationTasks(_ success: @escaping ([Task]?) -> Void) {
    
  }
  
  func deleteParticipant(_ userID: String, _ taskID: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func saveUserInTask(_ users: [User], _ taskID: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func registerUser(_ name: String, _ email: String, _ password: String, _ imageData: Data, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func loginUser(_ email: String, _ password: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func fetchAllUsers(_ success: @escaping ([User]?) -> Void) {
    
  }
  
  func fetchParticipants(_ id: String,_ success: @escaping ([User]?) -> Void) {
    
  }
  
  func save(_ task: Task, _ success: @escaping (Bool) -> Void) {
    NetworkService.shared.save(task, success: { boolean in
      print(task)
      success(boolean)
    }) { error in
      print(error)
    }
  }
  
  func fetchAll(_ success: @escaping ([Task]?) -> Void) {
    loadTasks { tasks in
      success(tasks)
    }
  }
  
  func fetchOne(_ index: String, _ success: @escaping (Task?) -> Void) {
    NetworkService.shared.taskByID(index, success: { task in
      success(task)
    }) { error in
      print(error)
    }
  }
  
  func deleteOne(_ index: String, _ success: @escaping (Bool) -> Void) {
    NetworkService.shared.deleteByID(index, success: { boolean in
      success(boolean)
    }) { error in
      print(error)
    }
  }
  
  func deleteAll(_ success: @escaping (Bool) -> Void) {
    
  }
  
  func update(_ task: Task, _ success: @escaping (Bool) -> Void) {
    NetworkService.shared.update(task, success: { boolean in
      success(true)
    }) { error in
      print(error)
    }
  }

  func loadTasks(_ success: @escaping ([Task]?) -> Void) {
    NetworkService.shared.getArr(success: { tasks in
      success(tasks)
    }) { error in
      print(error)
    }
  }
}
