//
//  Encodable Extensions.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/11/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import Foundation

enum MyError: Error {
  case encodingError
}

extension Encodable {
  func toJson(excluding keys: [String] = [String](), includingKeys: [String]? = [String](), uid: String?) throws -> [String: Any] {
    let objectData = try JSONEncoder().encode(self)
    let jsonObject = try JSONSerialization.jsonObject(with: objectData, options: [])
    guard var json = jsonObject as? [String: Any] else { throw MyError.encodingError }
    for key in keys {
      json[key] = nil
    }
    
    if let keys = includingKeys {
      for key in keys {
        json[key] = uid
      }
    }
    
    return json
  }
}
