//
//  DataStoreProtocol.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/26/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import Foundation

protocol DataStoreProtocol {
  func fetchUsersParticipationTasks(_ success: @escaping ([Task]?) -> Void)
  func deleteParticipant(_ userID: String, _ taskID: String, _ success: @escaping (Bool) -> Void)
  func saveUserInTask(_ users: [User], _ taskID: String, _ success: @escaping (Bool) -> Void)
  func registerUser(_ name: String, _ email: String, _ password: String, _ imageData: Data, _ success: @escaping (Bool) -> Void)
  func loginUser(_ email: String, _ password: String, _ success: @escaping (Bool) -> Void)
  func fetchAllUsers(_ success: @escaping ([User]?) -> Void)
  func fetchParticipants(_ id: String,_ success: @escaping ([User]?) -> Void)
  func save(_ task: Task, _ success: @escaping (Bool) -> Void)
  func fetchAll(_ success: @escaping ([Task]?) -> Void)
  func fetchOne(_ index: String, _ success: @escaping (Task?) -> Void)
  func deleteOne(_ index: String, _ success: @escaping (Bool) -> Void)
  func deleteAll(_ success: @escaping (Bool) -> Void)
  func update(_ task: Task, _ success: @escaping (Bool) -> Void)
  func loadTasks(_ success: @escaping ([Task]?) -> Void)
}
