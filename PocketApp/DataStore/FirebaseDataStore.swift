//
//  FirebaseDataStore.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/7/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class FirebaseDataStore: DataStoreProtocol {
  
  func fetchUsersParticipationTasks(_ success: @escaping ([Task]?) -> Void) {
    let ref = Database.database().reference()
    guard let currentUserID: String = Auth.auth().currentUser?.uid else { return }
    ref.child("tasksParticipants").child(currentUserID).observe(.value, with: { snapshot in
      var tasks: [Task] = []
      if let snapArr = snapshot.children.allObjects as? [DataSnapshot] {
        for snap in snapArr {
          if var dictionary = snap.value as? [String: Any] {
            dictionary.updateValue(snap.key, forKey: "taskID")
            do {
              let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
              let task = try JSONDecoder().decode(Task.self, from: jsonData)
              tasks.append(task)
            } catch {
              print(error)
            }
          }
        }
      }
      success(tasks)
    })
  }
  
  func deleteParticipant(_ userID: String, _ taskID: String, _ success: @escaping (Bool) -> Void) {
    let ref = Database.database().reference()
    let participantsRef = ref.child("tasks").child(taskID).child("participants")
    participantsRef.child(userID).removeValue()
    ref.child("tasksParticipants").child(userID).child(taskID).removeValue()
  }
  
  func saveUserInTask(_ users: [User], _ taskID: String, _ success: @escaping (Bool) -> Void) {
    let ref = Database.database().reference()
    let participantsRef = ref.child("tasks").child(taskID).child("participants")
    guard let currentUserID: String = Auth.auth().currentUser?.uid else { return }
    self.fetchOne(taskID) { fetchedTask in
      if let task = fetchedTask {
        do {
          for item in users {
            let participantJson = try item.toJson(excluding: ["userID"], includingKeys: nil, uid: nil)
            participantsRef.child(item.userID!).setValue(participantJson)
            
            let taskJson = try task.toJson(excluding: ["taskID", "participants", "creationDate", "details", "isNotificationEnabled", "scheduleDate"], includingKeys: ["createdBy"], uid: currentUserID)
            ref.child("tasksParticipants").child(item.userID!).child(taskID).setValue(taskJson)
          }
        } catch {
          print(error)
        }
      }
    }
  }
  
  func registerUser(_ name: String, _ email: String, _ password: String, _ imageData: Data, _ success: @escaping (Bool) -> Void) {
    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
      if error != nil {
        print(error)
        return
      }
      
      guard let uid = user?.user.uid else { return }
      
      let imageName = NSUUID().uuidString
      let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).png")
      
      storageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
        if error != nil {
          print(error)
          return
        }
        
        storageRef.downloadURL(completion: { (url, error) in
          if error != nil {
            print(error)
            return
          }
          
          if let profileImageUrl = url?.absoluteString {
            let values = ["name": name, "email": email, "profileImageUrl": profileImageUrl]
            self.registerUserIntoDatabaseWithUID(uid: uid, values: values)
            success(true)
          }
        })
      })
    })
  }
  
  private func registerUserIntoDatabaseWithUID(uid: String, values: [String: Any]) {
    let ref = Database.database().reference()
    let usersReference = ref.child("users").child(uid)
    
    usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
      if err != nil {
        print(err)
        return
      }
      print("Saved user successfully into firebase db")
    })
  }
  
  func loginUser(_ email: String, _ password: String, _ success: @escaping (Bool) -> Void) {
    Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
      if error != nil {
        print(error)
        return
      }
      success(true)
    }
  }
  
  func fetchAllUsers(_ success: @escaping ([User]?) -> Void) {
    let ref = Database.database().reference()
    ref.child("users").observe(.value, with: { (snapshot) in
      var users: [User] = []
      if let snapArr = snapshot.children.allObjects as? [DataSnapshot] {
        guard let currentUserID: String = Auth.auth().currentUser?.uid else { return }
        for snap in snapArr {
          if snap.key != currentUserID {
            if var dictionary = snap.value as? [String: Any] {
              dictionary.updateValue(snap.key, forKey: "userID")
              do {
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
                let user = try JSONDecoder().decode(User.self, from: jsonData)
                users.append(user)
              } catch {
                print(error)
              }
            }
          } else {
            debugPrint("current user filtered")
          }
        }
      }
      success(users)
    })
  }
  
  func fetchParticipants(_ id: String,_ success: @escaping ([User]?) -> Void) {
    let ref = Database.database().reference()
    ref.child("tasks").child(id).child("participants").observe(.value, with: { (snapshot) in
      var participants: [User] = []
      if let snapArr = snapshot.children.allObjects as? [DataSnapshot] {
        guard let currentUserID: String = Auth.auth().currentUser?.uid else { return }
        for snap in snapArr {
          if snap.key != currentUserID {
            if var dictionary = snap.value as? [String: Any] {
              dictionary.updateValue(snap.key, forKey: "userID")
              do {
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
                let user = try JSONDecoder().decode(User.self, from: jsonData)
                participants.append(user)
              } catch {
                print(error)
              }
            }
          } else {
            debugPrint("Current user filtered")
          }
        }
      }
      success(participants)
    })
  }
  
  func save(_ task: Task, _ success: @escaping (Bool) -> Void) {
    let ref = Database.database().reference()
    do {
      guard let uid = Auth.auth().currentUser?.uid else { return }
      
      let fullJson = try task.toJson(excluding: ["taskID"], includingKeys: ["createdBy"], uid: uid)
      
      let refAutoId = ref.child("tasks").childByAutoId()
      refAutoId.setValue(fullJson)
      
      let shortJson = try task.toJson(excluding: ["createdBy", "details"], includingKeys: nil, uid: nil)
      ref.child("userTasks").child(uid).child(refAutoId.key!).setValue(shortJson)
    } catch {
      print(error)
    }
  }
  
  func fetchAll(_ success: @escaping ([Task]?) -> Void) {
    loadTasks { tasks in
      success(tasks)
    }
  }
  
  func fetchOne(_ index: String, _ success: @escaping (Task?) -> Void) {
    let ref = Database.database().reference()
    ref.child("tasks").child(index).observeSingleEvent(of: .value) { (snapshot) in
      if var dictionary = snapshot.value as? [String: Any] {
        dictionary.updateValue(snapshot.key, forKey: "taskID")
        do {
          let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
          let task = try JSONDecoder().decode(Task.self, from: jsonData)
          success(task)
        } catch {
          print(error)
        }
      }
    }
  }
  
  func deleteOne(_ index: String, _ success: @escaping (Bool) -> Void) {
    let ref = Database.database().reference()
    guard let uid = Auth.auth().currentUser?.uid else { return }
    ref.child("tasks").child(index).removeValue()
    ref.child("userTasks").child(uid).child(index).removeValue()
  }
  
  func deleteAll(_ success: @escaping (Bool) -> Void) {
    
  }
  
  func update(_ task: Task, _ success: @escaping (Bool) -> Void) {
    let ref = Database.database().reference()
    do {
      let fullJson = try? task.toJson(excluding: ["taskID", "creationDate"], includingKeys: nil, uid: nil )
      ref.child("tasks").child(task.taskID!).updateChildValues(fullJson!)
      guard let uid = Auth.auth().currentUser?.uid else { return }
      let shortJson = try task.toJson(excluding: ["taskID", "createdBy", "details"], includingKeys: nil, uid: nil)
      ref.child("userTasks").child(uid).child(task.taskID!).updateChildValues(shortJson)
    } catch {
      print(error)
    }
    success(true)
  }
  
  func loadTasks(_ success: @escaping ([Task]?) -> Void) {
    let ref = Database.database().reference()
    guard let uid = Auth.auth().currentUser?.uid else { return }
    ref.child("userTasks").child(uid).observe(.value, with: { snapshot in
      var tasks: [Task] = []
      if let snapArr = snapshot.children.allObjects as? [DataSnapshot] {
        for snap in snapArr {
          if var dictionary = snap.value as? [String: Any] {
            dictionary.updateValue(snap.key, forKey: "taskID")
            do {
              let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
              let task = try JSONDecoder().decode(Task.self, from: jsonData)
              tasks.append(task)
            } catch {
              print(error)
            }
          }
        }
      }
      success(tasks)
    })
  }
}
