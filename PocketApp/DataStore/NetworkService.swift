//
//  NetworkDataStrore.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 12/24/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit
import Alamofire

class NetworkService {
  static let shared = NetworkService()
  private init() {}
  
  let url = "https://my-json-server.typicode.com/danchopon/myFakeAPI/tasks"
  
  func getArr(success: (([Task]) -> Void)?, failure: ((Error) -> Void)?) {
    AF.request(url)
      .responseJSON { response in
        switch response.result {
          case .success(_):
            guard let data = response.data else { return }
            do {
              let decoder = JSONDecoder()
              decoder.dateDecodingStrategy = .iso8601
              let tasks = try decoder.decode([Task].self, from: data)
              success!(tasks)
            } catch {
              print(error)
            }
          case .failure(_):
            print(response.result.error!)
        }
    }
  }
  
  func taskByID(_ id: String, success: ((Task) -> Void)?, failure: ((Error) -> Void)?) {
    let endpoint = Task.endpoindForID(id)
    AF.request(endpoint)
      .responseJSON { response in
        switch response.result {
        case .success(_):
          guard let data = response.data else { return }
          do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let task = try decoder.decode(Task.self, from: data)
            success!(task)
          } catch {
            print(error)
          }
        case .failure(_):
          print(response.result.error!)
        }
    }
  }
  
  func save(_ task: Task, success: ((Bool) -> Void), failure: ((Error) -> Void)?) {
    AF.request(url, method: .post)
      .responseJSON { response in
        switch response.result {
        case .success(_):
          print("save response is: \(response)")
        case .failure(_):
          print(response.result.error!)
        }
    }
  }
  
  func deleteByID(_ index: String, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
    let endpoint = Task.endpoindForID(index)
    AF.request(endpoint, method: .delete)
      .responseJSON { response in
        switch response.result {
        case .success(_):
          print("deleteByID response is: \(response)")
        case .failure(_):
          print(response.result.error!)
        }
    }
  }
  
  func update(_ task: Task, success: ((Bool) -> Void), failure: ((Error) -> Void)?) {
    let endpoint = Task.endpoindForID(task.taskID!)
    AF.request(endpoint, method: .put)
      .responseJSON { response in
        switch response.result {
        case .success(_):
          print("update response is: \(response)")
        case .failure(_):
          print(response.result.error!)
        }
    }
  }
}
