//
//  UDDataStore.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/26/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import Foundation

class UDDataStore: DataStoreProtocol {

  private let keyForUserDefaults = "myKey"
  
  func fetchUsersParticipationTasks(_ success: @escaping ([Task]?) -> Void) {
    
  }
  
  func deleteParticipant(_ userID: String, _ taskID: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func saveUserInTask(_ users: [User], _ taskID: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func registerUser(_ name: String, _ email: String, _ password: String, _ imageData: Data, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func loginUser(_ email: String, _ password: String, _ success: @escaping (Bool) -> Void) {
    
  }
  
  func fetchAllUsers(_ success: @escaping ([User]?) -> Void) {
    
  }
  
  func fetchParticipants(_ id: String,_ success: @escaping ([User]?) -> Void) {
    
  }
  
  private func decode2(_ data: Data, _ success: @escaping ([Task]?) -> Void) {
    success(try? PropertyListDecoder().decode([Task].self, from: data))
  }
  
  private func encode2(_ tasks: [Task], _ success: @escaping (Data?) -> Void ) {
    success(try? PropertyListEncoder().encode(tasks))
  }

  private func saveTasks2(_ tasks: [Task], _ success: @escaping (Bool) -> Void) {
    encode2(tasks) { encTasks in
      if let encodedTasks = encTasks {
        UserDefaults.standard.set(encodedTasks, forKey: self.keyForUserDefaults)
        success(true)
      } else {
        success(false)
      }
    }
  }
  
  func loadTasks(_ success: @escaping ([Task]?) -> Void) {
    if let encodedTasks = UserDefaults.standard.value(forKey: keyForUserDefaults) as? Data {
      decode2(encodedTasks) { tasks in
        success(tasks)
      }
    } else {
      success(nil)
    }
  }
  
  func save(_ task: Task, _ success: @escaping (Bool) -> Void) {
    var tasks = [Task]()
    loadTasks { decodedTasks in
      if let decodedTasks = decodedTasks {
        tasks = decodedTasks
      } else {
        tasks = [Task]()
      }
      tasks.append(task)
      self.saveTasks2(tasks, { boolean in
        success(boolean)
      })
    }
  }
  
  func fetchAll(_ success: @escaping ([Task]?) -> Void) {
    loadTasks { fetchedTasks in
      success(fetchedTasks)
    }
  }

  func fetchOne(_ index: String, _ success: @escaping (Task?) -> Void) {
    loadTasks{ loadedTasks in
      success(loadedTasks!.filter { $0.taskID == index }.first)
    }
  }
  
  func deleteOne(_ index: String, _ success: @escaping (Bool) -> Void) {
    loadTasks { decodedTasks in
      if var filteredArray = decodedTasks {
        filteredArray = decodedTasks!.filter { $0.taskID != index }
        self.saveTasks2(filteredArray, { boolean in
          success(boolean)
        })
      } else {
        success(false)
      }
    }
  }
  
  func deleteAll(_ success: @escaping (Bool) -> Void) {
    UserDefaults.standard.removeObject(forKey: keyForUserDefaults)
    success(true)
  }
  
  func update(_ task: Task, _ success: @escaping (Bool) -> Void) {
    loadTasks { decodedTasks in
      if var tasks = decodedTasks {
        if let row = tasks.index(where: { $0.taskID == task.taskID }) {
          tasks[row] = task
        }
          self.saveTasks2(tasks, { boolean in
            success(boolean)
          })
      } else {
        success(false)
      }
    }
  }
}
