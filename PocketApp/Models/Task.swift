//
//  Task.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/23/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit

struct Task: Codable {
  var taskID: String? = nil
  var taskLabel: String?
  var isDone: Bool?
  var creationDate: Date?
  var scheduledDate: Date?
  var isNotificationEnabled: Bool?
  var details: String?
  var createdBy: String?
  var participants: [User]?

  init(taskID: String?,
       taskLabel: String,
       isDone: Bool,
       creationDate: Date,
       scheduledDate: Date?,
       isNotificationEnabled: Bool,
       details: String?) {
    self.taskID = taskID
    self.taskLabel = taskLabel
    self.isDone = isDone
    self.creationDate = creationDate
    self.scheduledDate = scheduledDate
    self.isNotificationEnabled = isNotificationEnabled
    self.details = details
  }
  
  enum CodingKeys: String, CodingKey {
    case taskID //= "id"
    case taskLabel
    case isDone
    case creationDate
    case scheduledDate
    case isNotificationEnabled
    case details
    case createdBy
  }
  
  static func endpoindForID(_ id: String) -> String {
    return "https://my-json-server.typicode.com/danchopon/myFakeAPI/tasks/\(id)"
  }
}
