//
//  User.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 3/21/19.
//  Copyright © 2019 Daniyar Erkinov. All rights reserved.
//

import UIKit

struct User: Codable, Hashable, Equatable {
  var userID: String?
  var name: String?
  var email: String?
  var profileImageUrl: String?
}

func ==(lhs: User, rhs: User) -> Bool {
  return lhs.userID == rhs.userID &&
    lhs.name == rhs.name &&
    lhs.email == rhs.email &&
    lhs.profileImageUrl == rhs.profileImageUrl
}
