//
//  AppDelegate.swift
//  PocketApp
//
//  Created by Daniyar Erkinov on 11/22/18.
//  Copyright © 2018 Daniyar Erkinov. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var navigationController: UINavigationController?
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    
    if let window = window {
      let mainVC = TaskListViewController() 
      navigationController = UINavigationController(rootViewController: mainVC)
      window.rootViewController = navigationController
      window.makeKeyAndVisible()
    }
    
    UNUserNotificationCenter.current().delegate = self
    requestNotificationAuthorization()
    FirebaseApp.configure()
    return true
  }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    let detailsVC = TaskInfoViewController()
    let ident = response.notification.request.identifier
    if let taskId = ident as String? {
      detailsVC.currentTaskID = taskId
      self.navigationController?.pushViewController(detailsVC, animated: true)
    }
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler([.alert, .sound, .badge])
  }
}

func requestNotificationAuthorization() {
  UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) {
    (success, error) in
    if error != nil {
      print("Authorization Unsuccessfull")
    } else {
      print("Authorization Successfull")
    }
  }
}


